from api.users.models import Students, Users


def test_create_user(db):
    user = Users(
        password="test",
        email="test@email.com",
        created_at="2021-01-01",
        updated_at="2021-01-01",
        first_name="test",
        last_name="test",
    )
    with db() as session:
        session.add(user)
        session.commit()
        assert user.id == 1
        assert user.password == "test"
        assert user.email == "test@email.com"
        assert user.created_at == "2021-01-01"
        assert user.updated_at == "2021-01-01"
        assert user.first_name == "test"
        assert user.last_name == "test"


def test_create_student(db):
    student = Students(enrolllment_date="2021-01-01", min_course_credits=5, user_id=1)
    with db() as session:
        session.add(student)
        session.commit()
        assert student.id == 1
        assert student.enrolllment_date == "2021-01-01"
        assert student.min_course_credits == 5
        assert student.user_id == 1
