from datetime import datetime
from typing import Optional

from flask import jsonify, request
from flask_openapi3 import APIBlueprint
from pydantic import BaseModel
from sqlalchemy import select

from api.users.models import Users
from database import db

users_app = APIBlueprint("users_app", __name__)


class UserSchema(BaseModel):
    id: int
    password: str
    email: str
    created_at: str
    updated_at: str
    last_login: str
    first_name: str
    last_name: str


class UserList(BaseModel):
    users: list[UserSchema]


class StudentSchema(BaseModel):
    id: int
    enrolllment_date: str
    min_course_credits: str


@users_app.get("/users", responses={"200": UserList})
def get_users():
    with db.session() as session:
        users = session.execute(select(Users)).scalars().all()
        return jsonify([user.serialized for user in users])


@users_app.post("/user", responses={"201": UserSchema})
def create_user():
    user_info = request.get_json()

    with db.session() as session:
        user = Users(
            password=user_info["password"],
            email=user_info["email"],
            created_at=datetime.now(),
            updated_at=datetime.now(),
            last_login=datetime.now(),
            first_name=user_info["first_name"],
            last_name=user_info["last_name"],
        )
        session.add(user)
        session.commit()

        return jsonify(user)
